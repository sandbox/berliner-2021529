<?php

/**
 * @file
 * Defines list field types that can be used with the Options module.
 */

/**
 * Implements hook_help().
 */
function field_compliance_help($path, $arg) {
  switch ($path) {
    case 'admin/help#list':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The List module defines various fields for storing a list of items, for use with the Field module. Usually these items are entered through a select list, checkboxes, or radio buttons. See the <a href="@field-help">Field module help page</a> for more information about fields.', array('@field-help' => url('admin/help/field'))) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_field_info().
 */
function field_compliance_field_info() {
  return array(
    'field_compliance' => array(
      'label' => t('Compliance checkboxes'),
      'description' => t("This field provides a flexible list of checkboxes that need to be checked for compliance before an entity can be saved."),
      'settings' => array('allowed_values' => array(), 'error_message' => '', 'exclude_roles' => array(), 'allowed_values_function' => '', 'cardinality' => -1),
      'default_widget' => 'options_buttons',
      'default_formatter' => 'field_compliance_default',
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function field_compliance_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];

  switch ($field['type']) {
    case 'field_compliance':
      $form['allowed_values'] = array(
        '#type' => 'textarea',
        '#title' => t('Allowed values list'),
        '#description' => '<p>' . t('The possible values this field will show to the user. Enter one string per line. Each line will be rendered as an individual checkbox.') . '</p>',
        '#default_value' => field_compliance_allowed_values_string($settings['allowed_values']),
        '#rows' => 10,
        '#element_validate' => array('field_compliance_allowed_values_setting_validate'),
        '#field_has_data' => $has_data,
        '#field' => $field,
        '#field_type' => $field['type'],
        '#access' => empty($settings['allowed_values_function']),
      );

      $form['error_message'] = array(
        '#type' => 'textarea',
        '#title' => t('Error message'),
        '#default_value' => $settings['error_message'],
        '#description' => t('Define the error message that is shown when the entity is about to be saved and not all checkboxes are checked.'),
        '#rows' => 3,
        '#field_has_data' => $has_data,
        '#field' => $field,
        '#field_type' => $field['type'],
      );

      $form['exclude_roles'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Skip validation of this field for the following roles.'),
        '#description' => t('You can exclude specific roles from the requirement to comply with the options before saving an entity. Note that checking the <em>authenticated</em> role means that <em>all</em> authenticated users are given this right.'),
        '#default_value' => $settings['exclude_roles'],
        '#options' => user_roles(TRUE),
        '#field' => $field,
        '#field_type' => $field['type'],
      );
      break;

  }

  // Alter the description for allowed values depending on the widget type.
  if ($instance['widget']['type'] == 'options_buttons') {
    $form['allowed_values']['#description'] .= '<p>' . t("The 'checkboxes/radio buttons' widget will display checkboxes if the <em>Number of values</em> option is greater than 1 for this field, otherwise radios will be displayed.") . '</p>';
  }
  $form['allowed_values']['#description'] .= '<p>' . t('Allowed HTML tags in labels: @tags', array('@tags' => _field_filter_xss_display_allowed_tags())) . '</p>';

  $form['allowed_values_function'] = array(
    '#type' => 'value',
    '#value' => $settings['allowed_values_function'],
  );
  $form['allowed_values_function_display'] = array(
    '#type' => 'item',
    '#title' => t('Allowed values list'),
    '#markup' => t('The value of this field is being determined by the %function function and may not be changed.', array('%function' => $settings['allowed_values_function'])),
    '#access' => !empty($settings['allowed_values_function']),
  );

  return $form;
}

/**
 * Element validate callback; check that the entered values are valid.
 */
function field_compliance_allowed_values_setting_validate($element, &$form_state) {
  $field = $element['#field'];
  $has_data = $element['#field_has_data'];
  $field_type = $field['type'];
  $generate_keys = $field_type == 'field_compliance' && !$has_data;

  $values = field_compliance_extract_allowed_values($element['#value'], $field['type']);

  if (count($values) == 0) {
    form_error($element, t('Allowed values list: you must enter at least one value.'));
  }
  elseif (!is_array($values)) {
    form_error($element, t('Allowed values list: invalid input.'));
  }
  else {
    // Check that keys are valid for the field type.
    foreach ($values as $key => $value) {
      if ($field_type == 'field_compliance' && !preg_match('/^-?\d+$/', $key)) {
        form_error($element, t('Allowed values list: keys must be integers.'));
        break;
      }
    }
    form_set_value($element, $values, $form_state);
  }
}

/**
* Form element #value_callback: assembles the allowed values for 'boolean' fields.
*/
function field_compliance_boolean_allowed_values_callback($element, $input, $form_state) {
  $on = drupal_array_get_nested_value($form_state['input'], $element['#on_parents']);
  $off = drupal_array_get_nested_value($form_state['input'], $element['#off_parents']);
  return array($off, $on);
}

/**
 * Implements hook_field_update_field().
 */
function field_compliance_field_update_field($field, $prior_field, $has_data) {
  drupal_static_reset('field_compliance_allowed_values');
}

/**
 * Returns the array of allowed values for a list field.
 *
 * The strings are not safe for output. Keys and values of the array should be
 * sanitized through field_filter_xss() before being displayed.
 *
 * @param $field
 *   The field definition.
 * @param $instance
 *   (optional) A field instance array. Defaults to NULL.
 * @param $entity_type
 *   (optional) The type of entity; e.g. 'node' or 'user'. Defaults to NULL.
 * @param $entity
 *   (optional) The entity object. Defaults to NULL.
 *
 * @return
 *   The array of allowed values. Keys of the array are the raw stored values
 *   (number or text), values of the array are the display labels.
 */
function field_compliance_allowed_values($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
  $allowed_values = &drupal_static(__FUNCTION__, array());

  if (!isset($allowed_values[$field['id']])) {
    $function = $field['settings']['allowed_values_function'];
    // If $cacheable is FALSE, then the allowed values are not statically
    // cached. See field_compliance_test_dynamic_values_callback() for an example of
    // generating dynamic and uncached values.
    $cacheable = TRUE;
    if (!empty($function) && function_exists($function)) {
      $values = $function($field, $instance, $entity_type, $entity, $cacheable);
    }
    else {
      $values = $field['settings']['allowed_values'];
    }

    if ($cacheable) {
      $allowed_values[$field['id']] = $values;
    }
    else {
      return $values;
    }
  }

  return $allowed_values[$field['id']];
}

/**
 * Parses a string of 'allowed values' into an array.
 *
 * @param $string
 *   The list of allowed values in string format described in
 *   field_compliance_allowed_values_string().
 * @param $field_type
 *   The field type. Either 'field_compliance_number' or 'field_compliance_text'.
 *
 * @return
 *   The array of extracted key/value pairs, or NULL if the string is invalid.
 *
 * @see field_compliance_allowed_values_string()
 */
function field_compliance_extract_allowed_values($string, $field_type) {
  $values = array();

  $list = explode("\n", $string);
  $list = array_map('trim', $list);
  $list = array_filter($list, 'strlen');

  $generated_keys = $explicit_keys = FALSE;
  foreach ($list as $position => $text) {
    $value = $key = FALSE;

    // Check for an explicit key.
    $matches = array();
    if (preg_match('/(.*)\|(.*)/', $text, $matches)) {
      $key = $matches[1];
      $value = $matches[2];
      $explicit_keys = TRUE;
    }
    // Otherwise see if we can use the value as the key. Detecting true integer
    // strings takes a little trick.
    elseif ($field_type == 'field_compliance' && is_numeric($text) && (float) $text == intval($text)) {
      $key = $value = $text;
      $explicit_keys = TRUE;
    }
    // Otherwise see if we can generate a key from the position.
    else {
      $key = (string) $position;
      $value = $text;
      $generated_keys = TRUE;
    }

    $values[$key] = $value;
  }

  return $values;
}

/**
 * Generates a string representation of an array of 'allowed values'.
 *
 * This string format is suitable for edition in a textarea.
 *
 * @param $values
 *   An array of values, where array keys are values and array values are
 *   labels.
 *
 * @return
 *   The string representation of the $values array:
 *    - Values are separated by a carriage return.
 *    - Each value is in the format "value|label" or "value".
 */
function field_compliance_allowed_values_string($values) {
  $lines = array();
  foreach ($values as $key => $value) {
    $lines[] = "$key|$value";
  }
  return implode("\n", $lines);
}

/**
 * Implements hook_field_validate().
 *
 * Possible error codes:
 * - 'field_compliance_required': Not all checkboxes have been checked
 */
function field_compliance_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  $exclude_roles = array_filter($field['settings']['exclude_roles']);
  if (!empty($exclude_roles)) {
    global $user;
    $user_roles = $user->roles;
    // Authenticated is an authenticated user, independently of other roles.
    if (isset($exclude_roles[DRUPAL_AUTHENTICATED_RID]) && isset($user_roles[DRUPAL_AUTHENTICATED_RID])) {
      return;
    }
    // Check for corresponding additional roles.
    $count = count(array_intersect(array_filter($field['settings']['exclude_roles']), array_keys($user_roles)));
    if ($count != 0) {
      return;
    }
  }
  $allowed_values = field_compliance_allowed_values($field, $instance, $entity_type, $entity);
  $expected_keys = array_keys($allowed_values);
  $given_keys = array();
  foreach ($items as $delta => $item) {
    $given_keys[] = $item['value'];
  }
  
  if (count(array_diff($expected_keys, $given_keys))) {
    $errors[$field['field_name']][$langcode][$delta][] = array(
      'error' => 'field_compliance_required',
      'message' => t(check_plain($field['settings']['error_message'])),
    );
  }
}

/**
 * Implements hook_field_presave().
 */
function field_compliance_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
  // When all checkboxes have been checked we store 1, otherwhise 0.
  $expected_values = $field['settings']['allowed_values'];
  $all_checked = count($expected_values) == count($items);
  $items = array(0 => array('value' => $all_checked ? 1 : 0));
}

/**
 * Implements hook_field_is_empty().
 */
function field_compliance_field_is_empty($item, $field) {
  if (empty($item['value']) && (string) $item['value'] !== '0') {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_field_widget_info_alter().
 *
 * The List module does not implement widgets of its own, but reuses the
 * widgets defined in options.module.
 *
 * @see field_compliance_options_list()
 */
function field_compliance_field_widget_info_alter(&$info) {
  $widgets = array(
    'options_buttons' => array('field_compliance'),
  );

  foreach ($widgets as $widget => $field_types) {
    $info[$widget]['field types'] = array_merge($info[$widget]['field types'], $field_types);
  }
}

/**
 * Implements hook_options_list().
 */
function field_compliance_options_list($field, $instance, $entity_type, $entity) {
  return field_compliance_allowed_values($field, $instance, $entity_type, $entity);
}

/**
 * Implements hook_field_formatter_info().
 */
function field_compliance_field_formatter_info() {
  return array(
    'field_compliance_default' => array(
      'label' => t('Default'),
      'field types' => array('field_compliance'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function field_compliance_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'field_compliance_default':
      if (isset($items[0]['value']) && $items[0]['value'] == 1) {
        $output = t('All terms have been accepted');
        $element[0] = array('#markup' => $output);
      }
      break;

  }

  return $element;
}

/**
 * Implements hook_field_widget_form_alter().
 */
function field_compliance_field_widget_form_alter(&$element, &$form_state, $context) {
  if ($context['field']['module'] != 'field_compliance') {
    return;
  }
  
  if (strpos(implode('/', arg()), 'admin/structure/types/manage') !== FALSE) {
    return;
  }

  $field_name = $element['#field_name'];

  if (!isset($element['#entity']->$field_name)) {
    // This is the create entity form.
    return;
  }

  $delta = $element['#delta'];
  $items = field_get_items($element['#entity_type'], $element['#entity'], $field_name);
  if ($items[0]['value'] == 1) {
    // all checkboxes should be selected
    $element['#default_value'] = array();
    foreach ($element['#options'] as $key => $value) {
      $element['#default_value'][] = $key;
    }
  }
  else {
    // no checkbox should be selected
    foreach ($element['#options'] as $key => $value) {
      $element['#default_value'] = array();
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function field_compliance_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#field']['module'] != 'field_compliance') {
    return;
  }
  $form['instance']['required']['#access'] = FALSE;
  $form['instance']['default_value_widget']['#access'] = FALSE;
  $form['field']['cardinality']['#access'] = FALSE;
  $form['field']['cardinality']['#default_value'] = -1;
  $form['field']['cardinality']['#value'] = -1;
}
